using LF5_LGS;
using LF5_LGS.exceptions;
using LF5_LGS.models;
using NUnit.Framework;

namespace NUnitLgsTests.tests;

/// <summary>
///     Test class for LGS.
/// </summary>
public class LgsTest
{
    /// <summary>
    ///     Test if the LGS calculates expected results.
    /// </summary>
    [Test]
    public void TestLgs()
    {
        var resultVector = new Vector(new double[] {5, 6});
        var matrix = new Matrix(new[]
        {
            new double[] {2, 4},
            new double[] {2, 3}
        });

        var actual = Gauss.SolveLgs(matrix, resultVector);
        var expected = new Vector(new[] {4.5, -1});
        var invertedExpected = new Matrix(new[]
        {
            new[] {-1.5, 2},
            new double[] {1, -1}
        });
        var inverted = Gauss.SolveLgs(matrix);

        Assert.AreEqual(expected.GetData(), actual.GetData());
        Assert.AreEqual(invertedExpected.GetData(), inverted.GetData());
        Assert.AreEqual(new UnificationMatrix(2).GetData(), (inverted * matrix).GetData());
        Assert.AreEqual(new UnificationMatrix(2).GetData(), (matrix * inverted).GetData());
    }

    /// <summary>
    ///     Test if the Correct exception is Thrown if Solving LGS is not possible.
    /// </summary>
    [Test]
    public void TestInvalidLgs()
    {
        var resultVector = new Vector(new double[] {5, 6});
        var matrix = new Matrix(new[]
        {
            new double[] {2, 4},
            new double[] {2, 4}
        });

        Assert.Throws<UnsolvableMatrixException>(() => Gauss.SolveLgs(matrix, resultVector));
    }

    /// <summary>
    ///     Tests weather the correct exception is thrown if the matrix size exceeds the size of the vector.
    /// </summary>
    [Test]
    public void TestInvalidSizedMatrixLgs()
    {
        var resultVector = new Vector(new double[] {5, 6, 3});
        var matrix = new Matrix(new[]
        {
            new double[] {1, 2, 3},
            new double[] {1, 2, 3},
            new double[] {1, 2, 3},
            new double[] {1, 2, 3}
        });

        Assert.Throws<IllegalMatrixOperationException>(() => Gauss.SolveLgs(matrix, resultVector));
    }

    [Test]
    public void InvertMatrix()
    {
        var matrix = new Matrix(new[]
        {
            new double[] {2, 5},
            new double[] {1, 3}
        });
        var expected = new Matrix(new[]
        {
            new double[] {3, -5},
            new double[] {-1, 2}
        });

        var actual = Gauss.SolveLgs(matrix);
        var fromLeft = actual * matrix;
        var fromRight = matrix * actual;

        Assert.AreEqual(expected.GetData(), actual.GetData());
        Assert.AreEqual(new UnificationMatrix(2).GetData(), fromLeft.GetData());
        Assert.AreEqual(new UnificationMatrix(2).GetData(), fromRight.GetData());
    }
}