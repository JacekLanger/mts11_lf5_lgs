namespace LF5_LGS.exceptions;

public class IllegalMatrixOperationException : Exception
{
    public IllegalMatrixOperationException() : this("this operation is not valid.")
    {
    }

    public IllegalMatrixOperationException(string msg) : base(msg)
    {
    }
}