﻿using LF5_LGS.exceptions;
using LF5_LGS.models;

namespace LF5_LGS;

/// <summary>
///     Utility Class for solving Matrizes.This Class Provides an API for solving matrix equations.
/// </summary>
public static class Gauss
{
    /// <summary>
    ///     Solves a matrix with its respective solving vector, if the solving vector is not provided this
    ///     function returns the inverse of the matrix
    /// </summary>
    /// <param name="matrix"> the matrix to solve</param>
    /// <param name="result">
    ///     the result vector. If the result is null a Unification Matrix is used to calculate the Inverse of
    ///     the Matrix
    /// </param>
    /// <returns> a new vector containing the result or the inverse matrix if no vector was provided</returns>
    public static MatrixBase SolveLgs(MatrixBase matrix, MatrixBase? result = null)
    {
        MatrixBase res;

        if (result is not null)
        {
            if (result is Vector)
                res = Vector.Copy(result as Vector);
            // check if the matrix is solvable at all. ( matrix height == vector height )
            else res = result;
            if (matrix.Height > result.Height)
                throw new IllegalMatrixOperationException(
                    "the provided matrix can't be solved as its size exceeds the size of the result vector ");
        }
        else
        {
            res = new UnificationMatrix(matrix.Height);
        }

        var temp = Matrix.Copy(matrix);

        IterateDown(result, res, temp);
        IterateUp(result, res, temp);
        Normalize(temp, res);

        return res;
    }

    /// <summary>
    ///     Normalize the values of the matrix.
    /// </summary>
    /// <param name="temp"> the temporary matrix that is beeing returned</param>
    /// <param name="res"> the result matrix used to solve the lgs</param>
    /// <exception cref="UnsolvableMatrixException"> if the matrix is unsolvable</exception>
    private static void Normalize(MatrixBase temp, MatrixBase res)
    {
        for (var i = 0; i < temp.Height; i++)
        {
            if (temp[i].IsNullRow)
                throw new UnsolvableMatrixException();
            var alpha = 1 / temp[i][i];
            temp[i][i] *= alpha;
            if (res is not Vector)
                res[i] *= alpha;
            // res[i] = MatrixBase.Multiply(alpha, res[i]);
            else
                res[0][i] *= alpha;
        }
    }

    /// <summary>
    ///     Iterate over the matrix to set all the indizes on the left hand side of the matrix to 0.
    /// </summary>
    /// <param name="result">the result vector used to solve the matrix equation</param>
    /// <param name="res">the temporary result</param>
    /// <param name="matrix">the matrix to be solved</param>
    /// <exception cref="UnsolvableMatrixException">if matrix is unsolvable</exception>
    private static void IterateDown(MatrixBase? result, MatrixBase res, MatrixBase matrix)
    {
        for (var i = 0; i < matrix.Height - 1; i++)
        {
            // check if the next row contains a 0 if yes swap
            if (matrix[i + 1][i] == 0)
            {
                matrix.Swap(i, i + 1);
                res.Swap(i, i + 1);
                if (matrix[i + 1][i] == 0)
                    throw new UnsolvableMatrixException();
            }


            var topRow = matrix[i];
            for (var h = i; h < matrix.Height - 1; h++)
            {
                var currentRow = matrix[h + 1];
                var rowAlpha = -currentRow[i] / topRow[i];

                // there was an solving vector provided, therefore it needs to be updated. 
                if (result is not null && h == i)
                    res[0][i + 1] += rowAlpha * res[0][i];

                for (var j = i; j < matrix.Height; j++)
                {
                    // no solving vector was provided we use the unification vector
                    if (res is UnificationMatrix)
                        res[h + 1][j] +=
                            rowAlpha * res[i][j];

                    matrix[h + 1][j] = rowAlpha * topRow[j] + currentRow[j];
                }
            }
        }
    }

    /// <summary>
    ///     Iterate in reverse order over the matrix and set al the right hand values to 0 in order to achive a unification
    ///     matrix and the result vector.
    /// </summary>
    /// <param name="result">the result vector</param>
    /// <param name="res">the temporary result vector</param>
    /// <param name="matrix">the matrix to be solved</param>
    private static void IterateUp(MatrixBase? result, MatrixBase res, MatrixBase matrix)
    {
        for (var i = matrix.Height - 1; i >= 0; i--)
        {
            var topRow = matrix[i];
            for (var h = i; h >= 1; h--)
            {
                var currentRow = matrix[h - 1];
                var rowAlpha = -currentRow[i] / topRow[i];

                // there was an solving vector provided, therefore it needs to be updated. 
                if (result is not null && h == i)
                    res[0][i - 1] += rowAlpha * res[0][i];
                else if (h == i)
                    res[h - 1] += res[h] * rowAlpha;
                // res.Add(h - 1, MatrixBase.Multiply(rowAlpha, res[h]));

                for (var j = i; j >= 1; j--)
                    // no solving vector was provided we use the unification vector
                    matrix[h - 1][j] = rowAlpha * topRow[j] + currentRow[j];
            }
        }
    }
}