using LF5_LGS.models.data;

namespace LF5_LGS.models;

/// <summary>
///     Vector class. This is an abstraction of the Matrix class.
/// </summary>
public class Vector : MatrixBase
{
    /// <summary>
    ///     Creates a new Vector from the provided array.
    /// </summary>
    /// <param name="data"></param>
    public Vector(double[] data) : base(new double[1][])
    {
        Data[0] = new MatrixRow(data);
        Width = 1;
        Height = data.Length;
    }

    /// <summary>
    ///     Creates a new 0 vector of size n.
    /// </summary>
    /// <param name="n">the size of the vector</param>
    public Vector(int n) : base(1, n)
    {
        Height = n;
        Width = 1;
    }

    /// <summary>
    ///     Creates a new Vector from an existing vector.
    /// </summary>
    /// <param name="vector">the existing vector</param>
    public Vector(Vector vector) : this(vector.GetData()[0])
    {
    }

    /// <summary>
    ///     Square bracket accessor, provides array access for the data.
    /// </summary>
    /// <param name="i">the indexer</param>
    private new double this[int i]
    {
        get => Data[0][i];
        set => Data[0][i] = value;
    }

    /// <summary>
    ///     Returns the dimensions of the Vector.
    /// </summary>
    /// <returns>the dimension</returns>
    public int GetHeight()
    {
        return Data.GetLength(0);
    }

    /// <summary>
    ///     Creates a deep copy of a vector.
    /// </summary>
    /// <param name="vector">the vector to copy</param>
    /// <returns>new vector that is a deep copy of the provided vector</returns>
    public static Vector Copy(Vector vector)
    {
        var temp = new Vector(new double[vector.Height]);
        for (var j = 0; j < vector.Height; j++) temp[j] = vector[j];

        return temp;
    }
}