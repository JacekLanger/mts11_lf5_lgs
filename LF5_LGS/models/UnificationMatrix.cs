namespace LF5_LGS.models;

/// <summary>
///     Unification Matrix.
/// </summary>
public class UnificationMatrix : Matrix
{
    /// <summary>
    ///     Constructor will create a unification Matrix(a Square matrix where all values are 0 but the diagonals which ar 1).
    /// </summary>
    /// <param name="m">the size of the Matrix</param>
    public UnificationMatrix(int m) : base(m, m)
    {
        for (var i = 0; i < Data.Length; i++)
            Data[i][i] = 1;
    }
}