using LF5_LGS.models.data;

namespace LF5_LGS.models;

/// <summary>
///     A Matrix.This provides basic operations as multiply,add,divide and subtract
/// </summary>
public class Matrix : MatrixBase
{
    /// <summary>
    ///     Creates a new Matrix from a multidimensional array.
    /// </summary>
    /// <param name="data">the multidimensional array</param>
    public Matrix(double[][] data) : base(data)
    {
        Height = data.Length;
        Width = data.GetLength(0);
    }

    /// <summary>
    ///     Creates an empty matrix of the size m x n, all values will default to 0.
    /// </summary>
    /// <param name="m">width of the matrix</param>
    /// <param name="n">height of the matrix</param>
    public Matrix(int m, int n) : base(m, n)
    {
        Data = new MatrixRow[m];
        for (var i = 0; i < m; i++) Data[i] = new MatrixRow(n);
        Width = n;
        Height = m;
    }

    public static Matrix operator -(Matrix first, Matrix second)
    {
        return first + -1 * second;
    }

    /// <summary>
    ///     Multiplies an matrix with a alpha.
    /// </summary>
    /// <param name="alpha">the alpha</param>
    /// <param name="matrix">the matrix</param>
    /// <returns>result of the operation</returns>
    public static Matrix operator *(int alpha, Matrix matrix)
    {
        return (double) alpha * matrix;
    }

    /// <summary>
    ///     Multiplies an matrix with a alpha.
    /// </summary>
    /// <param name="alpha">the alpha</param>
    /// <param name="matrix">the matrix</param>
    /// <returns>result of the operation</returns>
    public static Matrix operator *(Matrix matrix, int alpha)
    {
        return (double) alpha * matrix;
    }

    /// <summary>
    ///     Multiplies an matrix with a alpha.
    /// </summary>
    /// <param name="alpha">the alpha</param>
    /// <param name="matrix">the matrix</param>
    /// <returns>result of the operation</returns>
    public static Matrix operator *(Matrix matrix, double alpha)
    {
        return alpha * matrix;
    }

    /// <summary>
    ///     Adds two matrices together and returns the result of that operation.
    /// </summary>
    /// <param name="first">the first matrix</param>
    /// <param name="second">the second matrix</param>
    /// <returns>the result of the operation</returns>
    public static Matrix operator +(Matrix first, MatrixBase second)
    {
        var temp = Copy(first);

        for (var i = second.Height - 1; i >= 0; i--)
        for (var j = second.GetData().Length - 1; j >= 0; j--)
            temp[i][j] += second[i][j];

        return temp;
    }


    /// <summary>
    ///     Multiplies a Matrix with a given alpha and returns the result.
    /// </summary>
    /// <param name="alpha"> factor by which the values are multiplied</param>
    /// <param name="matrix"> the matrix</param>
    /// <returns> new Matrix that is alhpa *  the old matrix</returns>
    public static Matrix operator *(double alpha, Matrix matrix)
    {
        Matrix temp = new(matrix.Height, matrix.Width);

        for (var i = 0; i < matrix.Height; i++)
        for (var j = 0; j < matrix.Width; j++)
            temp[i][j] = Math.Round(matrix[i][j] * alpha, 5);

        return temp;
    }

    /// <summary>
    ///     Returns a copy of the matrix as a new object. This new Object is no reference of the old one
    ///     but a completely new and independent object.
    /// </summary>
    /// <param name="matrix">matrix to copy</param>
    /// <returns> copy of the matrix</returns>
    public static Matrix Copy(MatrixBase matrix)
    {
        var temp = new double[matrix.Height][];
        for (var i = 0; i < matrix.Height; i++)
        {
            temp[i] = new double[matrix.Width];
            for (var j = 0; j < matrix.Width; j++) temp[i][j] = matrix[i][j];
        }

        return new Matrix(temp);
    }
}